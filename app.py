import flask

app = flask.Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    return "<h1>Hi there, Sumit</h1>"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
