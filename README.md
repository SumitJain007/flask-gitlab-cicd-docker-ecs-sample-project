Clone the repo

create virtual env and activate it 

pip freeze>requirements.txt

docker build -t flaskapp .

create a repo in docker hub wiht name flask-app-gitlab-cicd-ecs-app

docker tag flaskapp sumitjain12june/flask-app-gitlab-cicd-ecs-app:latest

docker push sumitjain12june/flask-app-gitlab-cicd-ecs-app:latest

create gitlab runner on your local - In terminal run the following-


    1. `brew install gitlab-runner`
    2. `brew services start gitlab-runner`
    3. `gitlab-runner register`
    4. Now register will gitlab project cicd runner 
        4.1 - using registration token (Project - settings - CICD - Expand Runners - Specific runner section
        4.2 - provide this in tags - `flask-gitlab-cicd-docker-ecsand`
        4.3 - provide `shell` as executor
    5. Once you local machine is registered as runner you will see that in Specific runner section
    6. tag provided at 4.2 will be used in .gilab-ci.yml file

